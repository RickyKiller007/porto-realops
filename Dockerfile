FROM python:3.8-slim

RUN \
    pip install --upgrade pip \
 && pip install gunicorn

RUN mkdir /app
WORKDIR /app

COPY requirements.txt /app/requirements.txt
RUN pip install -r requirements.txt

COPY src/ /app/src/

COPY wsgi.py /app/wsgi.py
EXPOSE 80/tcp
CMD ["gunicorn", "--bind=0.0.0.0:80", "--timeout", "180", "--log-file=-", "wsgi:app"]
