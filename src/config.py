from os import environ
from dotenv import load_dotenv

class Config:
    load_dotenv()
    DEBUG = False
    MONGODB_HOST = environ.get('MONGODB_HOST', 'db')
    MONGODB_DB = environ.get('MONGODB_DB', 'lppr-realops')
    SECRET_KEY = 'change-me'
    CLIENT_ID = environ.get('CLIENT_ID')
    CLIENT_SECRET = environ.get('CLIENT_SECRET')
    MAIL_SERVER = environ.get('MAIL_SERVER', 'localhost')
    MAIL_PORT = environ.get('MAIL_PORT', '')
    MAIL_USERNAME = environ.get('MAIL_USERNAME', '')
    MAIL_PASSWORD = environ.get('MAIL_PASSWORD', '')
    MAIL_DEFAULT_SENDER = 'noreply@portugal-vacc.org'
    MAIL_USE_TLS = 0

class Development(Config):
    MONGODB_HOST = 'localhost'
    DEBUG = True

class Staging(Config):
    MONGODB_HOST = 'db'
    DEBUG = True

class Production(Config):
    MONGODB_HOST = 'db'
    MAIL_SERVER = environ.get('MAIL_SERVER')
    MAIL_PORT = environ.get('MAIL_PORT')
    MAIL_USERNAME = environ.get('MAIL_USERNAME')
    MAIL_PASSWORD = environ.get('MAIL_PASSWORD')

configs = {
    'development': Development,
    'staging': Staging,
    'production': Production,
}

def configure_app(app, config):
    return app.config.from_object(configs.get(config, Config)())
