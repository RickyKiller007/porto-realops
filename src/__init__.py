import os
from flask import Flask
from flask_login import LoginManager
from flask_bootstrap import Bootstrap
from flask_oauthlib.client import OAuth
from flask_mongoengine import MongoEngine
from flask_mail import Mail

from .config import configure_app
from .routes import configure_routes
from .blueprints.auth.models import build_oauth_args

def configure_extensions(app):
    _oauth = OAuth(app)
    app.oauth = _oauth.remote_app('lppr_realops', **build_oauth_args(app))

    Bootstrap().init_app(app)
    LoginManager().init_app(app)
    MongoEngine().init_app(app)

    mail = Mail()
    mail.init_app(app)
    app.mail = mail
    
def configure_blueprints(app):
    from src.blueprints import auth_bp
    app.register_blueprint(auth_bp, url_prefix='/auth')

    from src.blueprints import flights_bp
    app.register_blueprint(flights_bp, url_prefix='/flights')

def configure_hooks(app):
    from src.blueprints import tokengetter, user_loader
    app.oauth.tokengetter(tokengetter)
    app.login_manager.user_loader(user_loader)

    from src.blueprints.flights.models import convert_from_file, init_from_file

    # Initialization
    convert_from_file('src/static/store/arrs', 'arrival')
    convert_from_file('src/static/store/deps', 'departure')

    #init_from_file('src/static/store/init_deps')
    #init_from_file('src/static/store/init_arrs')

def create_app(app_name):
    app = Flask(app_name, template_folder='src/templates', static_folder='src/static')

    configure_app(app, os.environ.get('FLASK_ENV'))
    configure_extensions(app)
    configure_blueprints(app)
    configure_hooks(app)
    configure_routes(app)

    return app
