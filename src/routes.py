from flask import redirect, url_for, render_template, request
from flask_login import login_required, current_user as user
from src.models import Mail
from .forms import ContactUsForm
from .blueprints.flights.entities import Flight

def configure_routes(app):
    @app.context_processor
    def main_args():
        return {
            'flights_booked': Flight.objects(book__cid=user.id),
        }

    @app.route('/')
    @login_required
    def index():
        perc_booked_deps = int(Flight.objects(
                book__ne=None, flight_type='departure').count() * 100 / \
            Flight.objects(flight_type='departure').count())
        perc_booked_arrs = int(Flight.objects(
                book__ne=None, flight_type='arrival').count() * 100 / \
            Flight.objects(flight_type='arrival').count())
        return render_template('index.html', perc_booked_deps=perc_booked_deps, perc_booked_arrs=perc_booked_arrs)

    @app.route('/contactus', methods=['POST', 'GET'])
    @login_required
    def contactus():
        senders = ['tmavicente@gmail.com', 'miguel.frias@portugal-vacc.org']
        form = ContactUsForm(user)
        if form.validate_on_submit():
            for s in senders:
                mail = Mail.contactus(
                    s, 
                    form.sender_name.data,
                    form.subject.data,
                    form.message.data,
                    form.sender_mail.data
                )
                app.mail.send(mail)
                return redirect(url_for('index'))
        return render_template('contactus.html', form=form)

    @app.errorhandler(401)
    def handle_401(error):
        return redirect(url_for('auth.login'))
    
    @app.errorhandler(400)
    def handle_400(error):
        return render_template('error.html', error=error)
    