from werkzeug import security

def build_oauth_args(app):
    return {
        'consumer_key'        : app.config['CLIENT_ID'],
        'consumer_secret'     : app.config['CLIENT_SECRET'],
        'request_token_params': {
            'scope': '', 
            'state': lambda: security.gen_salt(10)
        },
        'base_url'            : 'https://handover.portugal-vacc.org/',
        'authorize_url'       : 'https://handover.portugal-vacc.org/oauth/authorize',
        'request_token_url'   : None,
        'access_token_method' : 'POST',
        'access_token_url'    : 'https://handover.portugal-vacc.org/oauth/token',
        # force to parse the response in applcation/json
        'content_type'        : 'application/json',
    }
