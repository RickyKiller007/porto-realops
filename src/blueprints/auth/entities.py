from flask_login import UserMixin, login_user, logout_user, current_user
import mongoengine as me
from mongoengine.errors import DoesNotExist

from datetime import datetime

class _ControllerRating(me.EmbeddedDocument):
    id    = me.IntField()
    long  = me.StringField()
    short = me.StringField()

class _VatsimDetails(me.EmbeddedDocument):
    active_atc          = me.BooleanField()
    division            = me.StringField()
    controller_rating   = me.EmbeddedDocumentField(_ControllerRating)
    subdivision         = me.StringField()
    region              = me.StringField()
    visiting_controller = me.BooleanField()
    pilot_rating        = me.StringField()

class User(UserMixin, me.Document):
    id                 = me.IntField(primary_key=True)
    first_name         = me.StringField()
    last_name          = me.StringField()
    full_name          = me.StringField()
    email              = me.StringField()
    country            = me.StringField()
    vatsim_details     = me.EmbeddedDocumentField(_VatsimDetails)

    @staticmethod
    def current():
        return current_user

    @staticmethod
    def get(**kwargs):
        try:
            return User.objects.get(**kwargs)
        except DoesNotExist:
            pass

    def login(self):
        login_user(self)

    def logout(self):
        logout_user()
        
    @property
    def pilot_rating_code(self):
        _id = int(self.vatsim_details.pilot_rating)
        binary = format(_id, 'b')
        ratings = [f'P{i}' for i in range(1, 8)]
        ratings = [
            ratings[i]
            for i, rating in enumerate(binary)
            if int(rating) & 1
        ]
        return ratings[-1] if len(ratings) else 'P0'