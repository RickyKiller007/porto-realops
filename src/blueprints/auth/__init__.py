from flask import Blueprint, request, url_for, session, redirect, \
    render_template, current_app as app
from flask_login import current_user as user
import json
from .entities import User

bp = Blueprint('auth', __name__, template_folder='templates',
               static_folder='static')

def tokengetter():
    return session.get('access_token')

def user_loader(id):
    return User.get(id=id)

@bp.route('/login')
def login():
    return app.oauth.authorize(
        callback=url_for('auth.oauth_authorized', _external=True, _scheme='https'))

@bp.route('/oauth_authorized')
def oauth_authorized():
    resp = app.oauth.authorized_response()
    if resp is None:
        return 'Access denied: error=%s' % (
            request.args['error']
        )
    if isinstance(resp, dict) and 'access_token' in resp:
        session['access_token'] = (resp['access_token'], '')

        ret = app.oauth.get("/api/user")
        if ret.status not in (200, 201):
            return abort(ret.status)
        data = json.loads(ret.raw_data)['data']
        try:
            _user = User.objects.get(id=data['id'])
            _user.update(**data)
        except:
            _user = User(**data)
            _user.save()
        _user.login()

        return redirect(url_for('index'))
    return str(resp)

@bp.route('/logout')
def logout():
    user.logout()
    return redirect(url_for('index'))
