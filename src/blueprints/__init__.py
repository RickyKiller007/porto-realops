from .auth import bp as auth_bp, tokengetter, user_loader
from .flights import bp as flights_bp