from .entities import Flight

def convert_from_file(file, flight_type):
    with open(file, 'r') as file:
        for line in file.readlines():
            time, callsign, airline, location, aircraft = line.split()[:5]
            try:
                flight = Flight.objects.get(callsign=callsign)
                flight.update(
                    time=time,
                    airline=airline,
                    location=location,
                    aircraft=aircraft,
                    route=' '.join(line.split()[5:]),
                    flight_type=flight_type
                )
            except Exception as crap:
                flight = Flight(
                    time=time,
                    callsign=callsign,
                    airline=airline,
                    location=location,
                    aircraft=aircraft,
                    route=' '.join(line.split()[5:]),
                    flight_type=flight_type
                )
                flight.save()

def init_from_file(file):
    with open(file, 'r') as file:
        for line in file.readlines():
            callsign, cid = line.split()
            flight = Flight.objects.get(callsign=callsign)
            book = {'cid':cid}
            flight.update(book=book)