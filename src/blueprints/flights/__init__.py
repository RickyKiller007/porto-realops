from flask import Blueprint, render_template, redirect, url_for, current_app as app
from flask_login import login_required, current_user as user

from .entities import Flight
from ...models import Mail

bp = Blueprint('flights', __name__, template_folder='templates',
               static_folder='static')

@bp.route('/')
@login_required
def index():
    arrivals = Flight.objects(flight_type='arrival').order_by('time') 
    departures = Flight.objects(flight_type='departure').order_by('time') 
    return render_template('flights.html', arrivals=arrivals, departures=departures)

@bp.route('/<id>/book', methods=['POST'])
@login_required
def book(id):
    flight = Flight.get(id=id, book=None)
    if flight:
        book = {'cid': user.id}
        flight.update(book=book)

        user_mail = Mail.flight_booked_to_user(user.email, user.full_name, flight)
        staff_mail = Mail.flight_booked_alert_staff(user, flight)
        app.mail.send(user_mail)
        app.mail.send(staff_mail)

    return redirect(url_for('flights.index'))

@bp.route('/<id>/cancel_book')
@login_required
def cancel(id):
    flight = Flight.get(id=id, book__cid=user.id)
    if flight:
        flight.update(book=None)
    return redirect(url_for('flights.index'))