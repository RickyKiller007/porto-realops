import mongoengine as me
from mongoengine.errors import DoesNotExist

class Book(me.EmbeddedDocument):
    cid = me.IntField(required=True)

class Flight(me.Document):
    time         = me.StringField()
    callsign     = me.StringField(unique=True)
    airline      = me.StringField()
    location     = me.StringField()
    aircraft     = me.StringField()
    route        = me.StringField()
    flight_type  = me.StringField()
    book         = me.EmbeddedDocumentField(Book, default=None)

    @staticmethod
    def get(**kwargs):
        try:
            return Flight.objects.get(**kwargs)
            pass
        except DoesNotExist:
            pass

