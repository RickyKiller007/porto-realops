from flask import render_template
from flask_mail import Message

class Mail(Message):
    @classmethod
    def for_email(cls, email, subj, tmpl, **tmpl_kwargs):
        _tmpl = tmpl if '.html' not in tmpl else tmpl[:-5]
        html_body = render_template(f'{tmpl}.html', **tmpl_kwargs)
        return cls(subj, [email], None, html_body)

    @classmethod
    def contactus(cls, email, username, subject, message, sender_mail):
        tmpl = 'mail/contactus'
        subj = f'{subject} - ContactUs'
        return cls.for_email(email, subj, tmpl, username=username, message=message, _email=sender_mail)
    
    @classmethod
    def flight_booked_to_user(cls, email, username, flight):
        tmpl = f'mail/user_flight_booked_{flight.flight_type}'
        subj = f'Porto RealOps - Departure slot confirmation'
        return cls.for_email(email, subj, tmpl, username=username, flight=flight)

    @classmethod
    def flight_booked_alert_staff(cls, user, flight):
        tmpl = f'mail/staff_flight_booked_{flight.flight_type}'
        subj = f'Porto RealOps - {flight.callsign} {flight.flight_type.capitalize()} slot booking'
        return cls.for_email('events@portugal-vacc.org', subj, tmpl, user=user, flight=flight)
