from flask_wtf import FlaskForm
from wtforms import HiddenField, StringField, TextAreaField, SubmitField
from wtforms.widgets import TextArea

class ContactUsForm(FlaskForm):
    sender_mail  = HiddenField('Sender Mail')
    sender_name  = HiddenField('Sender Name')
    subject      = StringField('Subject')
    message      = StringField(u'Text', widget=TextArea())
    save         = SubmitField('Send')

    def __init__(self, user, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.sender_mail.data = user.email
        self.sender_name.data = user.full_name

